import 'package:flutter/material.dart';

class Config {
  static String appName = 'Eigital Code Test';

  static Color primaryColor = Color(0xFF0A529B);
  static Color secondaryColor = Color(0xFFA2D2F9);
  static Color tertiaryColor = Color(0xFF15B2D1);
  static Color darkColor = Color(0xFF313D49);
  static Color lightColor = Colors.grey[50];

  static Gradient primaryGradient = LinearGradient(
    colors: [
      Config.secondaryColor,
      Config.primaryColor,
    ],
  );

  // static String baseApiUrl = 'https://newsapi.org/v2';
  static String baseApiUrl = 'https://test.spaceflightnewsapi.net/api/v2';

  static String apiKey = '939238896f4c4f3d8ed5c3ef4ec1ab1a';

  static String mapBoxToken = 'pk.eyJ1IjoiZ2FicmllbGFybG8iLCJhIjoiY2tsYTFrNGo4MTU2MzJ2bnB0OXF5cXhqZCJ9.vTZcn-w4sgklkWCy57HtDA';
}
