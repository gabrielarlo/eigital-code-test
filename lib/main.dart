import 'dart:async';

import 'package:bot_toast/bot_toast.dart';
import 'package:eigital_code_test/pages/guarded/news_feeds.dart';
import 'package:eigital_code_test/pages/login.dart';
import 'package:eigital_code_test/services/auth_service.dart';
import 'package:eigital_code_test/services/helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive_listener/hive_listener.dart';
import 'package:provider/provider.dart';
import 'package:wakelock/wakelock.dart';

import 'env/config.dart';
import 'services/NavStack/custom_route_observer.dart';
import 'services/navigator_service.dart';
import 'services/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  setupLocator();

  await Hive.initFlutter();
  await Hive.openBox('Global', compactionStrategy: (entries, deletedEntries) => deletedEntries > 20);
  await Hive.openBox('Route', compactionStrategy: (entries, deletedEntries) => deletedEntries > 20);
  await Hive.openBox('Form', compactionStrategy: (entries, deletedEntries) => deletedEntries > 10);

  runZonedGuarded(() {
    runApp(MyApp());
  }, (error, stack) {
    print('runZonedGuarded Error: ' + error);
    print(stack);
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Wakelock.enable();

    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_) => AuthService(FirebaseAuth.instance),
        ),
        StreamProvider(
          create: (context) => context.read<AuthService>().authStateChanges,
        ),
      ],
      child: HiveListener(
        box: Helper.globalBox,
        keys: ['darkMode'],
        builder: (box) {
          bool darkMode = box.get('darkMode', defaultValue: false);

          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: Config.appName,
            theme: ThemeData(
              primaryColor: Config.primaryColor,
              backgroundColor: Colors.white,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              brightness: darkMode ? Brightness.dark : Brightness.light,
            ),
            builder: BotToastInit(),
            navigatorObservers: [
              BotToastNavigatorObserver(),
              CustomRouteObserver(),
            ],
            initialRoute: '/',
            onGenerateRoute: Routes.generateRoute,
            navigatorKey: locator<NavigationService>().navigatorKey,
          );
        },
      ),
    );
  }
}

class LandingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _firebaseUser = context.watch<User>();

    if (_firebaseUser != null) {
      print(_firebaseUser);
      return NewsFeedsPage();
    } else {
      return LoginPage();
    }
  }
}
