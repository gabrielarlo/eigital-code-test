import 'package:eigital_code_test/services/dio/api_response.dart';
import 'package:eigital_code_test/services/dio/dio_client.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:simple_moment/simple_moment.dart';

class NewsModel {
  final String id;
  final String author;
  final String title;
  final String description;
  final String url;
  final String urlToImage;
  final String publishedAt;

  NewsModel({this.id, this.author, this.title, this.description, this.url, this.urlToImage, this.publishedAt});

  factory NewsModel.fromJson(Map<String, dynamic> json) {
    String publishedAt;
    var moment = Moment.now();
    DateTime published = DateTime.parse(json['publishedAt']);
    publishedAt = moment.from(published);

    return NewsModel(
      id: json['id'],
      author: json['author'] ?? json['newsSite'],
      title: json['title'],
      description: json['description'] ?? json['summary'],
      url: json['url'],
      urlToImage: json['imageUrl'] ?? json['urlToImage'].toString().replaceAll('http://', 'https://'),
      publishedAt: publishedAt,
    );
  }

  static Future<List> list({String category: 'articles'}) async {
    // String uri = '/everything';
    String uri = '/${category.toLowerCase()}';
    print(uri);
    APIResponse res = await DioClient().publicGet(uri, data: {});
    if (res.code != 200) return [];
    // return res.data['articles'];
    return res.data;
  }

  static Future<Map> item({@required String id}) async {
    String uri = '/articles/' + id;
    APIResponse res = await DioClient().publicGet(uri, data: {});
    if (res.code != 200) return null;
    return res.data;
  }
}
