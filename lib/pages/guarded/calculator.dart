import 'package:auto_size_text/auto_size_text.dart';
import 'package:eigital_code_test/env/config.dart';
import 'package:eigital_code_test/services/helper.dart';
import 'package:eigital_code_test/services/operator.dart';
import 'package:eigital_code_test/widgets/e_drawer.dart';
import 'package:flutter/material.dart';
import 'package:hive_listener/hive_listener.dart';

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  List<Map> _operators = [
    {'symbol': '%', 'onTap': Operator.percent},
    {'symbol': '()', 'onTap': Operator.brackets},
    {'symbol': '÷', 'onTap': Operator.divide},
    {'symbol': 'x', 'onTap': Operator.multiply},
    {'symbol': '-', 'onTap': Operator.minus},
    {'symbol': '+', 'onTap': Operator.plus},
    {'symbol': 'DEL', 'onTap': Operator.delete},
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      drawer: EDrawer(),
      appBar: AppBar(
        title: Text('Calculator'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Expanded(
                      child: HiveListener(
                        box: Helper.globalBox,
                        keys: ['operationEquation'],
                        builder: (box) {
                          return AutoSizeText(
                            Operator.getEquation().replaceAll('/', '÷'),
                            style: TextStyle(fontSize: 60.0, letterSpacing: 6.0),
                            maxLines: 4,
                            textAlign: TextAlign.end,
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                children: [
                  Expanded(
                    child: HiveListener(
                      box: Helper.globalBox,
                      keys: ['simplify'],
                      builder: (box) {
                        return AutoSizeText(
                          box.get('simplify', defaultValue: ''),
                          style: TextStyle(fontSize: 8.0),
                          maxLines: 1,
                          textAlign: TextAlign.end,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: Row(
                children: [
                  Expanded(
                    child: HiveListener(
                      box: Helper.globalBox,
                      keys: ['operationResult'],
                      builder: (box) {
                        return AutoSizeText(
                          Operator.getResult(),
                          style: TextStyle(fontSize: 60.0),
                          maxLines: 1,
                          textAlign: TextAlign.end,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Divider(thickness: 2.0),
            IntrinsicHeight(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 3,
                    child: FractionallySizedBox(
                      widthFactor: 1.0,
                      child: Wrap(
                        children: List.generate(12, (index) {
                          return _BigButton(
                            size: size,
                            index: index,
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      mainAxisSize: MainAxisSize.max,
                      children: _operators.map((e) {
                        return Expanded(
                          child: _SmallButton(symbol: e['symbol'], onTap: e['onTap']),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _SmallButton extends StatelessWidget {
  final String symbol;
  final Function onTap;

  const _SmallButton({
    Key key,
    @required this.symbol,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        color: symbol == 'DEL' ? Config.secondaryColor : Colors.transparent,
        child: Center(
          child: Text(
            symbol,
            style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold, letterSpacing: 8.0),
          ),
        ),
      ),
    );
  }
}

class _BigButton extends StatelessWidget {
  const _BigButton({
    Key key,
    @required this.size,
    @required this.index,
  }) : super(key: key);

  final Size size;
  final int index;

  @override
  Widget build(BuildContext context) {
    int num = 0;
    if (index < 3) {
      num = 7 + index;
    } else if (index < 6) {
      num = 1 + index;
    } else if (index < 9) {
      num = index - 5;
    }
    String char = index == 9
        ? 'C'
        : index == 11
            ? '.'
            : num.toString();

    return InkWell(
      onTap: () {
        Operator.input(char);
      },
      child: Container(
        width: size.width * 0.25,
        height: size.width * 0.20,
        color: char == 'C' ? Config.secondaryColor : Colors.transparent,
        child: Center(
          child: Text(
            char,
            style: TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
