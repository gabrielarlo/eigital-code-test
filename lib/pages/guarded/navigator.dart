import 'dart:math';

import 'package:eigital_code_test/env/config.dart';
import 'package:eigital_code_test/widgets/e_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mapbox_navigation/library.dart';
import 'package:location/location.dart';
import 'package:maps_toolkit/maps_toolkit.dart';

class NavigatorPage extends StatefulWidget {
  @override
  _NavigatorPageState createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage> {
  Location _location = new Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  double _latitude;
  double _longitude;
  double _newLatitude;
  double _newLongitude;

  MapBoxNavigation _directions;
  MapBoxOptions _options;
  String _platformVersion = 'Unknown';
  String _instruction = '';
  bool _arrived = false;
  bool _isMultipleStop = false;
  double _distanceRemaining, _durationRemaining;
  MapBoxNavigationViewController _controller;
  bool _routeBuilt = false;
  bool _isNavigating = false;
  double _distanceBetween = 0.0;

  @override
  void initState() {
    _getPermissions();

    super.initState();
    _initializeMap();
  }

  Future<void> _getPermissions() async {
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        // return;
      }
    }

    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        // return;
      }
    }

    _locationData = await _location.getLocation();
    _location.changeSettings(distanceFilter: 10);
    _latitude = _locationData.latitude;
    _longitude = _locationData.longitude;
  }

  Future<void> _initializeMap() async {
    if (!mounted) return;
    _directions = MapBoxNavigation(onRouteEvent: _onRouteEvent);
    _options = MapBoxOptions(
      initialLatitude: _latitude,
      initialLongitude: _longitude,
      zoom: 11.0,
      tilt: 0.0,
      bearing: 0.0,
      enableRefresh: true,
      voiceInstructionsEnabled: true,
      bannerInstructionsEnabled: true,
      allowsUTurnAtWayPoints: true,
      mode: MapBoxNavigationMode.drivingWithTraffic,
      units: VoiceUnits.metric,
      simulateRoute: true,
      animateBuildRoute: true,
      longPressDestinationEnabled: true,
      language: 'en',
      isOptimized: true,
      alternatives: true,
    );

    String platformVersion;
    try {
      platformVersion = await _directions.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version';
    }
    setState(() {
      _platformVersion = platformVersion;
    });
  }

  _onRouteEvent(RouteEvent event) {}

  @override
  void dispose() {
    _controller.clearRoute();
    _directions.finishNavigation();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: EDrawer(),
      appBar: AppBar(
        title: Text('Navigator'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(8.0),
            color: Config.secondaryColor,
            child: Center(child: Text('Running on: $_platformVersion [${_distanceBetween.toStringAsFixed(2)} km]')),
          ),
          Row(
            children: [
              TextButton(
                onPressed: () async {
                  Map newLocation = _randomLocation();
                  _newLatitude = newLocation['lat'];
                  _newLongitude = newLocation['lng'];

                  List<WayPoint> wayPoints = [];
                  wayPoints.add(WayPoint(name: 'Origin', latitude: _latitude, longitude: _longitude));
                  wayPoints.add(WayPoint(name: 'Random New Location', latitude: _newLatitude, longitude: _newLongitude));
                  _controller.buildRoute(wayPoints: wayPoints);

                  _distanceBetween = SphericalUtil.computeDistanceBetween(LatLng(_latitude, _longitude), LatLng(_newLatitude, _newLongitude));
                  _distanceBetween = _distanceBetween / 1000;

                  setState(() {});
                },
                child: Text('Build Route'),
              ),
              Expanded(child: SizedBox()),
              TextButton(
                onPressed: _routeBuilt && !_isNavigating
                    ? () async {
                        List<WayPoint> wayPoints = [];
                        wayPoints.add(WayPoint(name: 'Origin', latitude: _latitude, longitude: _longitude));
                        wayPoints.add(WayPoint(name: 'Random New Location', latitude: _newLatitude, longitude: _newLongitude));

                        await _directions.startNavigation(
                          wayPoints: wayPoints,
                          options: _options,
                        );
                      }
                    : null,
                child: Text('Start'),
              ),
            ],
          ),
          Expanded(
            child: Container(
              color: Colors.grey,
              child: MapBoxNavigationView(
                options: _options,
                onRouteEvent: _onEmbeddedRouteEvent,
                onCreated: (MapBoxNavigationViewController controller) async {
                  _controller = controller;
                  _controller.initialize();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Map _randomLocation() {
    Random random = new Random();
    double u = random.nextDouble();
    double v = random.nextDouble();
    double w = 0.1 * sqrt(u);
    double t = 2 * pi * v;
    double x = w * cos(t);
    double y = w * sin(t);

    return {'lng': x + _longitude, 'lat': y + _latitude};
  }

  Future<void> _onEmbeddedRouteEvent(e) async {
    _distanceRemaining = await _directions.distanceRemaining;
    _durationRemaining = await _directions.durationRemaining;

    switch (e.eventType) {
      case MapBoxEvent.progress_change:
        var progressEvent = e.data as RouteProgressEvent;
        _arrived = progressEvent.arrived;
        if (progressEvent.currentStepInstruction != null) _instruction = progressEvent.currentStepInstruction;
        break;
      case MapBoxEvent.route_building:
      case MapBoxEvent.route_built:
        setState(() {
          _routeBuilt = true;
        });
        break;
      case MapBoxEvent.route_build_failed:
        setState(() {
          _routeBuilt = false;
        });
        break;
      case MapBoxEvent.navigation_running:
        setState(() {
          _isNavigating = true;
        });
        break;
      case MapBoxEvent.on_arrival:
        _arrived = true;
        if (!_isMultipleStop) {
          await Future.delayed(Duration(seconds: 3));
          await _controller.finishNavigation();
        } else {}
        break;
      case MapBoxEvent.navigation_finished:
        setState(() {
          _isNavigating = false;
          _routeBuilt = false;
        });
        break;
      case MapBoxEvent.navigation_cancelled:
        setState(() {
          _routeBuilt = false;
          _isNavigating = false;
        });
        break;
      default:
        break;
    }
    setState(() {});
  }
}
