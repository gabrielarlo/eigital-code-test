import 'package:eigital_code_test/env/config.dart';
import 'package:eigital_code_test/models/news_model.dart';
import 'package:eigital_code_test/services/helper.dart';
import 'package:eigital_code_test/widgets/e_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive_listener/hive_listener.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsFeedsPage extends StatefulWidget {
  @override
  _NewsFeedsPageState createState() => _NewsFeedsPageState();
}

class _NewsFeedsPageState extends State<NewsFeedsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: EDrawer(),
      appBar: AppBar(
        title: Text('News Feeds'),
        centerTitle: true,
        bottom: PreferredSize(
          child: Container(
            color: Config.secondaryColor,
            height: 40.0,
            child: Row(
              children: [
                _CategoryButton(category: 'Articles'),
                VerticalDivider(),
                _CategoryButton(category: 'Blogs'),
                VerticalDivider(),
                _CategoryButton(category: 'Reports'),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(40.0),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {});
        },
        child: HiveListener(
          box: Helper.globalBox,
          keys: ['currentCategory'],
          builder: (box) {
            String _category = box.get('currentCategory', defaultValue: 'Articles');

            return FutureBuilder(
              future: NewsModel.list(category: _category),
              builder: (c, s) {
                if (s.hasData) {
                  List list = s.data;

                  if (list.length == 0) return Center(child: Text('No Content'));
                  return ListView.builder(
                    shrinkWrap: true,
                    padding: EdgeInsets.all(16.0),
                    itemCount: list.length,
                    itemBuilder: (c, i) {
                      NewsModel news = NewsModel.fromJson(list[i]);

                      if (news.title == null) return SizedBox();

                      return _NewsCard(news: news);
                    },
                  );
                }
                return LinearProgressIndicator();
              },
            );
          },
        ),
      ),
    );
  }
}

class _CategoryButton extends StatelessWidget {
  final String category;

  const _CategoryButton({
    Key key,
    @required this.category,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: () {
          Helper.globalBox.put('currentCategory', category);
        },
        child: Container(
          child: Center(
            child: HiveListener(
              box: Helper.globalBox,
              keys: ['currentCategory'],
              builder: (box) {
                String _category = box.get('currentCategory', defaultValue: 'Articles');
                return Text(
                  category,
                  style: TextStyle(fontWeight: FontWeight.bold, color: _category == category ? Colors.white : Colors.black),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

class _NewsCard extends StatelessWidget {
  const _NewsCard({
    Key key,
    @required this.news,
    this.single: false,
  }) : super(key: key);

  final NewsModel news;
  final bool single;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: single ? EdgeInsets.zero : EdgeInsets.only(bottom: 16.0),
      clipBehavior: Clip.antiAlias,
      child: InkWell(
        onTap: single
            ? null
            : () {
                showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  builder: (c) {
                    return Wrap(
                      children: [
                        _NewsCard(news: news, single: true),
                      ],
                    );
                  },
                );
              },
        child: Padding(
          padding: single ? EdgeInsets.symmetric(horizontal: 16.0) : EdgeInsets.zero,
          child: Column(
            children: [
              single
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        IconButton(
                            icon: Icon(Icons.close_rounded),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }),
                      ],
                    )
                  : SizedBox(),
              ListTile(
                title: Text(
                  news.title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 8.0),
                    news.author != null
                        ? Text(
                            news.author,
                            style: TextStyle(fontStyle: FontStyle.italic, fontSize: 16.0),
                          )
                        : SizedBox(),
                    news.publishedAt != null ? Text(news.publishedAt) : SizedBox(),
                  ],
                ),
              ),
              Divider(),
              Image.network(news.urlToImage),
              SizedBox(height: 8.0),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  news.description,
                  textAlign: TextAlign.justify,
                ),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Ref: '),
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          launch(news.url);
                        },
                        child: Text(
                          news.url,
                          style: TextStyle(color: Colors.blue),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
