import 'package:eigital_code_test/env/config.dart';
import 'package:eigital_code_test/services/goto.dart';
import 'package:eigital_code_test/services/helper.dart';
import 'package:eigital_code_test/widgets/e_button.dart';
import 'package:eigital_code_test/widgets/e_input.dart';
import 'package:eigital_code_test/widgets/e_logo.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email = '';
  String _password = '';
  String _error = '';

  @override
  void initState() {
    Map formData = Helper.fillForm(formKey: 'loginForm', fields: {
      'email': '',
      'password': '',
    });
    _email = formData['email'];
    _password = formData['password'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: Config.tertiaryColor,
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: FractionallySizedBox(
                widthFactor: 1.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ELogo(),
                    SizedBox(height: 16.0),
                    FractionallySizedBox(
                      widthFactor: 0.7,
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Column(
                            children: [
                              EInput(
                                label: 'Email',
                                value: _email,
                                isEmail: true,
                                onChanged: (val) {
                                  _email = val;
                                },
                                onSubmitted: (val) async {
                                  _email = val;
                                  await _signIn(email: _email, password: _password);
                                },
                              ),
                              EInput(
                                label: 'Password',
                                value: _password,
                                isPassword: true,
                                onChanged: (val) {
                                  _password = val;
                                },
                                onSubmitted: (val) async {
                                  _password = val;
                                  await _signIn(email: _email, password: _password);
                                },
                              ),
                              SizedBox(height: 16.0),
                              Text(
                                _error,
                                style: TextStyle(color: Colors.red),
                                textAlign: TextAlign.center,
                              ),
                              EButton(
                                label: 'Sign In',
                                onTap: () async {
                                  await _signIn(email: _email, password: _password);
                                },
                                full: true,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future _signIn({@required String email, @required String password}) async {
    Helper.formBox.put('loginForm', {'email': _email, 'password': _password});
    String res = await Helper.auth().signIn(email: email, password: password);
    if (res == 'signed-in') Goto.root('/news-feeds');
    setState(() {
      _error = res;
    });
  }
}
