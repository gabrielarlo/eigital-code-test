import 'package:eigital_code_test/services/helper.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class AuthService {
  final FirebaseAuth _firebaseAuth;

  AuthService(this._firebaseAuth);

  Stream<User> get authStateChanges {
    return _firebaseAuth.authStateChanges();
  }

  Future<String> signIn({@required String email, @required String password}) async {
    print('signIn');
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      Helper.user = _firebaseAuth.currentUser.email;
      return 'signed-in';
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
    Helper.globalBox.delete('user');
  }
}
