import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:eigital_code_test/env/config.dart';

import 'api_response.dart';
import 'dio_connectivity.dart';
import 'dio_interceptor.dart';

class DioClient {
  Dio _dio = Dio();

  DioClient() {
    _dio.options.baseUrl = Config.baseApiUrl;
    _dio.options.connectTimeout = 30000;
    _dio.options.receiveTimeout = 10000;

    _dio.interceptors.add(RetryOnConnectionChangeInterceptor(
      requestRetrier: DioConnectivityRequestRetrier(
        dio: _dio,
        connectivity: Connectivity(),
      ),
    ));
  }

  Future<APIResponse> publicGet(String uri, {Map<String, dynamic> data, bool inBackground: false}) async {
    print('publicGet');
    _dio.options.headers["content-type"] = 'application/json';

    final response = await this._dio.get(uri, queryParameters: data).catchError((error) {print(error);});
    return _process(response);
  }

  Future<APIResponse> publicPost(String uri, {Map<String, dynamic> data, bool inBackground: false}) async {
    _dio.options.headers["content-type"] = 'application/json';

    FormData formData = new FormData.fromMap(data);
    final response = await this._dio.post(
      uri,
      data: formData,
      onSendProgress: (int sent, int total) {
        if (sent == total) print('Full Sent');
      },
      onReceiveProgress: (int received, int total) {
        if (received == total) print('Full Received');
      },
    ).catchError((error) {
      print(error);
    });
    return _process(response);
  }

  APIResponse _process(Response response) {
    if (response == null) {
      return APIResponse.fromJson({
        'code': 500,
        'msg': 'Server cannot be reach.',
        'data': null,
      });
    }
    return APIResponse.fromJson({
      'code': 200,
      'msg': response.statusMessage,
      'data': response.data,
    });
  }
}
