import 'package:eigital_code_test/services/auth_service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

class Helper {
  static Box globalBox = Hive.box('Global');
  static Box routeBox = Hive.box('Route');
  static Box formBox = Hive.box('Form');

  static Map<String, dynamic> fillForm({@required String formKey, @required Map fields}) {
    Map<String, dynamic> form = {};

    Map formBox = Helper.formBox.get(formKey, defaultValue: {});
    fields.forEach((key, value) {
      form[key] = formBox[key] ?? value;
    });

    return form;
  }

  static set user(String user) {
    print(user);
    globalBox.put('user', user);
  }

  static String get user {
    return globalBox.get('user');
  }

  static AuthService auth() => AuthService(FirebaseAuth.instance);

  static String number(double num) {
    var _formattedNumber = NumberFormat().format(num);
    return _formattedNumber;
  }
}
