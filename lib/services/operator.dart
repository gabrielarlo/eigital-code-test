import 'package:eigital_code_test/services/helper.dart';
import 'package:math_expressions/math_expressions.dart';

class Operator {
  static bool _operatorLast = false;
  static bool _openBracket = false;
  static List<String> _operators = ['%', '()', '÷', 'x', '-', '+'];

  static Parser get _parser {
    return Parser();
  }

  static void _operand(String _operand) {
    print(_operand);
    if (!_operatorLast) {
      String equation = getEquation();
      String lastChar = equation.substring(equation.length - 1);
      bool _valid = true;
      if (lastChar == '(' && _operand != '-') {
        _valid = false;
      }
      if (_valid) {
        _operatorLast = true;
        equation += _operand;
        Helper.globalBox.put('operationEquation', equation);
      } else {
        print('Invalid sequence');
      }
    }
  }

  static void percent() {
    print('percent');
    _operand('%');
  }

  static void brackets() {
    print('brackets');
    _openBracket = !_openBracket;
    _operatorLast = false;
    String equation = getEquation();
    equation += _openBracket ? '(' : ')';
    Helper.globalBox.put('operationEquation', equation);
    _calculate();
  }

  static void divide() {
    print('divide');
    _operand('/');
  }

  static void multiply() {
    print('multiply');
    _operand('*');
  }

  static void minus() {
    print('minus');
    _operand('-');
  }

  static void plus() {
    print('plus');
    _operand('+');
  }

  static void delete() {
    print('delete');
    String equation = getEquation();
    if (equation.length > 1) {
      String removedLastChar = equation.substring(equation.length - 1);
      equation = equation.substring(0, equation.length - 1);
      String lastChar = equation.substring(equation.length - 1);
      _operatorLast = _operators.contains(lastChar);
      if (removedLastChar == '(') {
        _openBracket = false;
      } else if (removedLastChar == ')') {
        _openBracket = true;
      }
    } else {
      equation = '';
      _operatorLast = false;
      _openBracket = false;
    }
    Helper.globalBox.put('operationEquation', equation);
    _calculate();
  }

  static String getResult() {
    double saveResult = Helper.globalBox.get('operationResult', defaultValue: 0.0);
    return '= ' + Helper.number(saveResult);
  }

  static String getEquation() {
    String equation = '';
    equation = Helper.globalBox.get('operationEquation', defaultValue: equation);
    return equation;
  }

  static void input(String char) {
    _operatorLast = false;
    if (char == 'C') {
      _operatorLast = false;
      _openBracket = false;
      Helper.globalBox.put('operationEquation', '');
      Helper.globalBox.put('operationResult', 0.0);
    } else if (char == '.') {
      String equation = getEquation();
      if (!equation.contains('.')) {
        equation += char;
        Helper.globalBox.put('operationEquation', equation);
        _calculate();
      }
    } else {
      String equation = getEquation();
      equation += char;
      Helper.globalBox.put('operationEquation', equation);
      _calculate();
    }
  }

  static void _calculate() {
    String equation = getEquation();
    print(equation);
    try {
      if (equation == '') {
        Helper.globalBox.put('operationResult', 0.0);
      } else {
        String tempEquation = equation;
        while (tempEquation.indexOf('(') >= 0 && tempEquation.indexOf(')') >= 0) {
          String inside = '';
          int openIndex = 0;
          int closeIndex = 0;

          openIndex = tempEquation.indexOf('(');
          closeIndex = tempEquation.indexOf(')');
          inside = tempEquation.substring(openIndex + 1, closeIndex);
          // print(openIndex);
          // print(closeIndex);
          print(inside);
          Expression tempExp = _parser.parse(inside);
          print(tempExp);
          double tempEval = tempExp.evaluate(EvaluationType.REAL, ContextModel());
          print(tempEval);

          tempEquation = tempEquation.replaceFirst('($inside)', '*${tempEval.toString()}');
          print(tempEquation);
        }

        equation = tempEquation;

        Expression exp = _parser.parse(equation);
        print(exp);
        print(exp.simplify());

        double eval = exp.evaluate(EvaluationType.REAL, ContextModel());
        print(eval);

        Helper.globalBox.put('simplify', exp.simplify().toString());
        Helper.globalBox.put('operationResult', eval);
      }
    } catch (e) {
      print('Incomplete');
    }
  }
}
