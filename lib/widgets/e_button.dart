import 'package:eigital_code_test/env/config.dart';
import 'package:flutter/material.dart';

class EButton extends StatelessWidget {
  final String label;
  final Function onTap;
  final bool full;

  const EButton({
    Key key,
    @required this.label,
    this.onTap,
    this.full: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget button = ElevatedButton(
      onPressed: onTap,
      style: ButtonStyle(
        backgroundColor: MaterialStateColor.resolveWith((states) => Config.primaryColor),
      ),
      child: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
    );

    if (full) {
      return FractionallySizedBox(
        widthFactor: 1.0,
        child: button,
      );
    }
    return button;
  }
}
