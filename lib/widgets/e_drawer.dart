import 'package:eigital_code_test/env/config.dart';
import 'package:eigital_code_test/services/goto.dart';
import 'package:eigital_code_test/services/helper.dart';
import 'package:eigital_code_test/widgets/e_logo.dart';
import 'package:flutter/material.dart';
import 'package:hive_listener/hive_listener.dart';

class EDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            child: Column(
              children: [
                ELogo(),
                Expanded(child: SizedBox()),
                Text(
                  Helper.user ?? '',
                  style: TextStyle(color: Config.secondaryColor, fontSize: 18.0),
                ),
              ],
            ),
            decoration: BoxDecoration(
              color: Config.primaryColor,
            ),
          ),
          ListTile(
            leading: Icon(Icons.rss_feed),
            title: Text('News Feeds'),
            onTap: () {
              Navigator.of(context).pop();
              Goto.push('/news-feeds');
            },
          ),
          ListTile(
            leading: Icon(Icons.compass_calibration_outlined),
            title: Text('Navigator'),
            onTap: () {
              Navigator.of(context).pop();
              Goto.push('/navigator');
            },
          ),
          ListTile(
            leading: Icon(Icons.calculate_outlined),
            title: Text('Calculator'),
            onTap: () {
              Navigator.of(context).pop();
              Goto.push('/calculator');
            },
          ),
          HiveListener(
              box: Helper.globalBox,
              keys: ['darkMode'],
              builder: (box) {
                bool darkMode = box.get('darkMode', defaultValue: false);

                return ListTile(
                  leading: Icon(darkMode ? Icons.lightbulb : Icons.lightbulb_outline),
                  title: Text('Dark Mode'),
                  trailing: Icon(darkMode ? Icons.check_box : Icons.check_box_outline_blank),
                  onTap: () {
                    box.put('darkMode', !darkMode);
                  },
                );
              }),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Sign Out'),
            onTap: () async {
              await Helper.auth().signOut();
              Navigator.of(context).pop();
              Goto.root('/');
            },
          ),
        ],
      ),
    );
  }
}
