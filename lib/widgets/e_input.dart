import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class EInput extends StatefulWidget {
  final String label;
  final String value;
  final Function(String) onChanged;
  final Function(String) onSubmitted;
  final bool isPassword;
  final bool isEmail;

  const EInput({
    Key key,
    @required this.label,
    @required this.value,
    this.onChanged,
    this.onSubmitted,
    this.isPassword: false,
    this.isEmail: false,
  }) : super(key: key);

  @override
  _EInputState createState() => _EInputState();
}

class _EInputState extends State<EInput> {
  String _value = '';
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    _value = widget.value;
    _controller = TextEditingController()..text = _value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String error;
    if (widget.isEmail && _value.length > 0) {
      bool valid = EmailValidator.validate(_value);
      if (!valid) error = 'Invalid Email Format';
    }

    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 16.0, top: 8.0),
          child: TextField(
            controller: _controller,
            decoration: InputDecoration(
              hintText: '',
              errorText: error,
            ),
            obscureText: widget.isPassword,
            onChanged: (val) {
              setState(() {
                _value = val;
                if (widget.onChanged != null) widget.onChanged(_value);
              });
            },
            onSubmitted: widget.onSubmitted,
          ),
        ),
        Positioned(
          child: Text(
            widget.label,
            style: TextStyle(fontSize: 12.0),
          ),
        ),
      ],
    );
  }
}
