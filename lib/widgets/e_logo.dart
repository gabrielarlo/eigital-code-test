import 'package:flutter/material.dart';

class ELogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      'Eigital Test'.toUpperCase(),
      style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 32.0, letterSpacing: 4.0),
    );
  }
}
